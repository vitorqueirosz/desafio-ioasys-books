import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import * as reactRedux from 'react-redux';
import userEvent from '@testing-library/user-event';
import SignIn from '../../../pages/SignIn';
import { store } from '../../../store';

describe('Test signIn page successfully', () => {
  it('shoul render SignIn page', () => {
    const { container } = render(
      <Provider store={store}>
        <SignIn />
      </Provider>,
    );

    expect(container).toMatchSnapshot();
  });

  it('shoul dispatch a action with signIn payload', async () => {
    const mockedDispatch = jest.fn();
    const useDispatch = jest.spyOn(reactRedux, 'useDispatch');
    useDispatch.mockReturnValue(mockedDispatch);

    render(
      <Provider store={store}>
        <SignIn />
      </Provider>,
    );

    userEvent.type(screen.getByPlaceholderText(/E-mail/i), 'teste@gmail.com');
    userEvent.type(screen.getByPlaceholderText(/Senha/i), 'password123123');

    const signInButton = screen.getByRole('button', { name: /Entrar/i });

    userEvent.click(signInButton);

    await waitFor(() => expect(mockedDispatch).toHaveBeenCalledWith({
      error: undefined,
      meta: undefined,
      payload: {
        email: 'teste@gmail.com',
        password: 'password123123',
      },
      type: '@auth/SIGN_IN_REQUEST',
    }));
  });
});
