import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Input from '../../../components/form/Input';

describe('Test input successfully', () => {
  it('should render a input successfully', () => {
    const { container } = render(<Input label="Input test" />);

    const input = screen.getByRole('textbox');

    expect(container).toMatchSnapshot();
    expect(input).toBeInTheDocument();
  });

  it('should input a value when user types', () => {
    render(<Input label="Input test" />);

    const input = screen.getByRole('textbox');

    userEvent.type(input, 'input-test');

    expect(input.value).toBe('input-test');
  });
});
