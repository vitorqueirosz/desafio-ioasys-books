import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Button from '../../../components/structure/Button';

describe('Test button successfullly', () => {
  it('should render a button successfullly', () => {
    const { container } = render(<Button>Entrar</Button>);

    const button = screen.getByRole('button', { name: 'Entrar' });

    expect(button).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('should call a button click successfullly', () => {
    const onClick = jest.fn();

    render(<Button onClick={onClick}>Entrar</Button>);

    const button = screen.getByRole('button', { name: 'Entrar' });

    userEvent.click(button);

    expect(onClick).toHaveBeenCalled();
  });
});
