import { runSaga } from 'redux-saga';
import api from '../../../../services/api';
import { signInFailure, signInSuccess } from '../../../../store/modules/auth/actions';
import { signIn } from '../../../../store/modules/auth/sagas';
import { User } from '../../../../store/modules/signOut/types';

jest.mock('../../../../services/api');

describe('Test auth sagas successfully', () => {
  it('should dispatch signInSuccess action when saga execute correctly', async () => {
    const headers = { authorization: 'any-token', 'refresh-token': 'any-token' };
    const data = {} as User;

    const successPayload = { authorization_token: 'any-token', user: {}, refreshToken: 'any-token' };

    const mockedRequest = api as jest.Mocked<typeof api>;
    mockedRequest.post.mockResolvedValue({ headers, data });

    const dispatched: any[] = [];

    await runSaga({
      dispatch: (action) => dispatched.push(action),
    }, signIn as any, { type: '@auth/SIGN_IN_REQUEST', payload: { email: 'teste@g.com', password: 'teste123123' } }).toPromise();

    const returnedReducerState = signInSuccess(successPayload);

    expect(dispatched).toEqual([returnedReducerState]);
  });

  it('should dispatch signInSuccess action when saga execute correctly', async () => {
    const mockedRequest = api as jest.Mocked<typeof api>;
    mockedRequest.post.mockRejectedValue({
      response: {
        status: 401,
      },
    });

    const dispatched: any[] = [];

    await runSaga({
      dispatch: (action) => dispatched.push(action),
    }, signIn as any, { type: '@auth/SIGN_IN_REQUEST', payload: { email: 'teste@g.com', password: 'teste123123' } }).toPromise();

    const returnedReducerState = signInFailure({ message: 'Email e/ou senha incorretos.' });

    expect(dispatched).toEqual([returnedReducerState]);
  });
});
