import { User } from '../../../../store/modules/signOut/types';

import reducer from '../../../../store/modules/auth/reducer';

describe('Test auth reducers successfully', () => {
  it('should return the correct state when call action with type @auth/SIGN_IN_REQUEST', () => {
    const previousState = {
      loadingSignInRequest: false,
      error: false,
      authorization_token: null,
      errMessage: null,
      user: {} as User,
      refreshToken: null,
    };

    const expectedState = {
      error: false,
      authorization_token: null,
      errMessage: null,
      user: {} as User,
      refreshToken: null,
      loadingSignInRequest: true,
    };

    const payload = {
      email: 'teste@gmail.com',
      password: 'test123123',
    };

    const response = reducer(previousState, {
      type: '@auth/SIGN_IN_REQUEST',
      payload,
    });

    expect(response).toEqual(expectedState);
  });

  it('should return the correct state when call action with type @auth/SIGN_IN_SUCCESS', () => {
    const payload = { authorization_token: 'any-token', user: {}, refreshToken: 'any-token' };

    const previousState = {
      loadingSignInRequest: false,
      error: false,
      authorization_token: null,
      errMessage: null,
      user: {} as User,
      refreshToken: null,
    };

    const expectedState = {
      error: false,
      authorization_token: 'any-token',
      errMessage: null,
      user: {} as User,
      refreshToken: 'any-token',
      loadingSignInRequest: false,
    };

    const response = reducer(previousState, {
      type: '@auth/SIGN_IN_SUCCESS',
      payload,
    });

    expect(response).toEqual(expectedState);
  });

  it('should return the correct state when call action with type @auth/SIGN_IN_FAILURE', () => {
    const payload = { message: 'error-message' };

    const previousState = {
      loadingSignInRequest: false,
      error: false,
      authorization_token: null,
      errMessage: null,
      user: {} as User,
      refreshToken: null,
    };

    const expectedState = {
      error: true,
      authorization_token: null,
      errMessage: 'error-message',
      user: {} as User,
      refreshToken: null,
      loadingSignInRequest: false,
    };

    const response = reducer(previousState, {
      type: '@auth/SIGN_IN_FAILURE',
      payload,
    });

    expect(response).toEqual(expectedState);
  });
});
