import { signInFailure, signInRequest, signInSuccess } from '../../../../store/modules/auth/actions';

describe('Test auth actions successfully', () => {
  it('should return correct value from signInRequest action', () => {
    const email = 'test@gmail.com';
    const password = 'password123';

    const response = signInRequest({ email, password });

    const expectedResult = {
      error: undefined,
      meta: undefined,
      payload: {
        email: 'test@gmail.com',
        password: 'password123',
      },
      type: '@auth/SIGN_IN_REQUEST',
    };

    expect(response).toStrictEqual(expectedResult);
  });

  it('should return correct value from signInSuccess action', () => {
    const payload = { authorization_token: 'any-token', user: {}, refreshToken: 'any-token' };

    const response = signInSuccess(payload);

    const expectedResult = {
      error: undefined,
      meta: undefined,
      payload: {
        authorization_token: 'any-token',
        user: {},
        refreshToken: 'any-token',
      },
      type: '@auth/SIGN_IN_SUCCESS',
    };

    expect(response).toStrictEqual(expectedResult);
  });

  it('should return correct value from signInRequest action', () => {
    const email = 'test@gmail.com';
    const password = 'password123';

    const response = signInRequest({ email, password });

    const expectedResult = {
      error: undefined,
      meta: undefined,
      payload: {
        email: 'test@gmail.com',
        password: 'password123',
      },
      type: '@auth/SIGN_IN_REQUEST',
    };

    expect(response).toStrictEqual(expectedResult);
  });

  it('should return correct value from signInFailure action', () => {
    const response = signInFailure({ message: 'Invalid credentials' });

    const expectedResult = {
      error: undefined,
      meta: undefined,
      payload: {
        message: 'Invalid credentials',
      },
      type: '@auth/SIGN_IN_FAILURE',
    };

    expect(response).toStrictEqual(expectedResult);
  });
});
