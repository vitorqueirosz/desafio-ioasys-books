import { AnyAction, Middleware, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore } from 'redux-persist';

import rootSaga from './modules/rootSaga';
import persistReducers from './persistReducers';
import createStore from './createStore';
import { rootReducer } from './modules/rootReducer';

const sagaMiddleware = createSagaMiddleware();

const middlewares: Middleware[] = [sagaMiddleware];

const store = createStore(persistReducers(rootReducer), middlewares);

sagaMiddleware.run(rootSaga);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const persistor = persistStore((store as unknown) as Store<any, AnyAction>);

export { store, persistor };
