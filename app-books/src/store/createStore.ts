import {
  applyMiddleware,
  createStore,
  Middleware,
  Reducer,
  Store,
} from 'redux';
import { PersistState } from 'redux-persist';
import { PersistPartial } from 'redux-persist/es/persistReducer';
import { AuthAction, AuthState } from './modules/auth/types';
import { BookAction, BookState } from './modules/book/types';
import { BooksAction, BooksState } from './modules/books/types';
import { SignOutAction, SignOutState } from './modules/signOut/types';

export interface StoreState {
  _persist: PersistState;
  auth: AuthState & PersistPartial;
  books: BooksState;
  book: BookState;
  signOut: SignOutState;
}

export type StoreAction = AuthAction | BooksAction | BookAction | SignOutAction;

export default (
  reducers: Reducer<StoreState, StoreAction>,
  middlewares: Middleware[],
): Store => {
  const enhancer = applyMiddleware(...middlewares);

  return createStore(reducers, enhancer);
};
