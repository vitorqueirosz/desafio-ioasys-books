import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import auth from './auth/reducer';
import signOut from './signOut/reducer';

import books from './books/reducer';
import book from './book/reducer';

const authConfig = {
  key: 'auth',
  storage,
  blacklist: ['errMessage'],
};

const rootReducer = combineReducers({
  _persist: (state: any = null) => state,
  auth: persistReducer(authConfig, auth),
  books,
  book,
  signOut,
});

export type ApplicationState = ReturnType<typeof rootReducer>;

export { rootReducer };
