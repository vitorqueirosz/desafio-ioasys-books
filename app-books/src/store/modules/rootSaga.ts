import { all } from 'redux-saga/effects';
import auth from './auth/sagas';
import signOut from './signOut/sagas';
import books from './books/sagas';
import book from './book/sagas';

export default function* root() {
  yield all([
    auth,
    books,
    book,
    signOut,
  ]);
}
