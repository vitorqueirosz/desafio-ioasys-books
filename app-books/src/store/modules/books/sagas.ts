import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

import * as signOutActions from '../signOut/actions';

export function* signIn({ payload }: ActionType<typeof actions.fetchBooksRequest>) {
  try {
    const {
      amount, category, page, refreshToken,
    } = payload;

    const response = yield call(api.get, 'books', {
      params: {
        page,
        amount,
      },
    });

    const { totalPages } = response.data;

    const books = response.data.data;

    yield put(actions.fetchBooksSuccess({ books, totalPages: Math.ceil(totalPages) }));
  } catch (err) {
    if (err.response.status === 400 || err.response.status === 401) {
      yield put(signOutActions.signOutRequest());
    }
    yield put(actions.fetchBooksFailure({}));
  }
}

export default all([takeLatest('@books/FETCH_BOOKS_REQUEST', signIn)]);
