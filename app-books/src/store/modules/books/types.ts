import { ActionType } from 'typesafe-actions';

import * as actions from './actions';

export type BooksAction = ActionType<typeof actions>;

export interface BooksState {
  readonly loadingSignInRequest: boolean;
  readonly error: boolean;
  readonly refreshToken: string | null;
  readonly page: number | null;
  readonly amount?: number | null;
  readonly totalPages?: number | null;
  readonly category?: string | null;
  readonly books: Book[];
  readonly errMessage?: string | null;

}

export interface Book {
  id: string;
  authors: string[];
  imageUrl: string;
  author: string;
  description: string;
  title: string;
  published: string;
  pageCount: string;
  publisher: string;
  isbn13: string;
  isbn10: string;
  language: string;
}
