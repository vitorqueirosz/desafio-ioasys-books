import { action } from 'typesafe-actions';
import { Book } from './types';

export function fetchBooksRequest({
  refreshToken,
  page,
  amount,
  category,
}: {
  refreshToken?: string;
  page: number;
  amount: number;
  category?: string;
}) {
  return action('@books/FETCH_BOOKS_REQUEST', {
    refreshToken, page, amount, category,
  });
}

export function fetchBooksSuccess({ books, totalPages }: { books: Book[]; totalPages: number }) {
  return action('@books/FETCH_BOOKS_SUCCESS', { books, totalPages });
}

export function fetchBooksReset() {
  return action('@books/FETCH_BOOKS_RESET');
}

export function fetchBooksFailure({ message }: { message?: string }) {
  return action('@books/FETCH_BOOKS_FAILURE', { message });
}
