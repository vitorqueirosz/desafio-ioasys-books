import { BooksAction, BooksState } from './types';

const INTIAL_STATE: BooksState = {
  loadingSignInRequest: false,
  error: false,
  refreshToken: null,
  errMessage: null,
  page: null,
  amount: null,
  category: null,
  books: [],
  totalPages: null,
};

export default function auth(
  state = INTIAL_STATE,
  action: BooksAction,
): BooksState {
  switch (action.type) {
    case '@books/FETCH_BOOKS_REQUEST':
      return {
        ...state,
        loadingSignInRequest: true,
      };

    case '@books/FETCH_BOOKS_SUCCESS':
      return {
        ...state,
        loadingSignInRequest: false,
        books: action.payload.books,
        totalPages: action.payload.totalPages,
        errMessage: null,
      };

    case '@books/FETCH_BOOKS_RESET':
      return {
        ...state,
        loadingSignInRequest: false,
        books: [],
        errMessage: null,
      };

    case '@books/FETCH_BOOKS_FAILURE':
      return {
        ...state,
        loadingSignInRequest: false,
        error: true,
        errMessage: action.payload.message,
      };

    default:
      return state;
  }
}
