import { action } from 'typesafe-actions';
import { Book } from '../books/types';

export function fetchBookRequest({
  id,
}: {
  id: string
}) {
  return action('@book/FETCH_BOOK_REQUEST', { id });
}

export function fetchBookSuccess({ book }: { book: Book }) {
  return action('@book/FETCH_BOOK_SUCCESS', { book });
}

export function fetchBookReset() {
  return action('@book/FETCH_BOOK_RESET');
}

export function fetchBookFailure({ message }: { message?: string }) {
  return action('@book/FETCH_BOOK_FAILURE', { message });
}
