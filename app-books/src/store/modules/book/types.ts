import { ActionType } from 'typesafe-actions';
import { Book } from '../books/types';

import * as actions from './actions';

export type BookAction = ActionType<typeof actions>;

export interface BookState {
  readonly loadingBookRequest: boolean;
  readonly error: boolean;
  readonly authorization_token: string | null;
  readonly errMessage?: string | null;
  readonly book: Book;
  readonly id: string | null;
}
