import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

export function* fetchBook({ payload }: ActionType<typeof actions.fetchBookRequest>) {
  try {
    const { id } = payload;

    console.log('id', payload);

    const response = yield call(api.get, `books/${id}`);

    const book = response.data;

    yield put(actions.fetchBookSuccess({ book }));
  } catch (err) {
    yield put(actions.fetchBookFailure({}));
  }
}

export default all([takeLatest('@book/FETCH_BOOK_REQUEST', fetchBook)]);
