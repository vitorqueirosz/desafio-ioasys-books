import { Book } from '../books/types';
import { BookAction, BookState } from './types';

const INTIAL_STATE: BookState = {
  loadingBookRequest: false,
  error: false,
  authorization_token: null,
  errMessage: null,
  book: {} as Book,
  id: null,
};

export default function auth(
  state = INTIAL_STATE,
  action: BookAction,
): BookState {
  switch (action.type) {
    case '@book/FETCH_BOOK_REQUEST':
      return {
        ...state,
        id: action.payload.id,
        loadingBookRequest: true,
      };

    case '@book/FETCH_BOOK_SUCCESS':
      return {
        ...state,
        loadingBookRequest: false,
        book: action.payload.book,
        errMessage: null,
      };

    case '@book/FETCH_BOOK_RESET':
      return {
        ...state,
        loadingBookRequest: false,
        errMessage: null,
      };

    case '@book/FETCH_BOOK_FAILURE':
      return {
        ...state,
        loadingBookRequest: false,
        error: true,
        errMessage: action.payload.message,
      };

    default:
      return state;
  }
}
