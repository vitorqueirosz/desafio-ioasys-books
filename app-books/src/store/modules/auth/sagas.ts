import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

export function* signIn({ payload }: ActionType<typeof actions.signInRequest>) {
  try {
    const { email, password } = payload;

    const { data, headers } = yield call(api.post, 'auth/sign-in', { email, password });

    const { authorization } = headers;

    const refreshToken = headers['refresh-token'];

    api.defaults.headers.Authorization = `Bearer ${authorization}`;

    yield put(
      actions.signInSuccess({ authorization_token: authorization, user: data, refreshToken }),
    );
  } catch (err) {
    if (err.response.status === 401) {
      yield put(actions.signInFailure({ message: 'Email e/ou senha incorretos.' }));
    } else {
      yield put(actions.signInFailure({}));
    }
  }
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);
