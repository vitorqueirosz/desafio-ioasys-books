import { AuthAction, AuthState, User } from './types';

const INTIAL_STATE: AuthState = {
  loadingSignInRequest: false,
  error: false,
  authorization_token: null,
  errMessage: null,
  user: {} as User,
  refreshToken: null,
};

export default function auth(
  state = INTIAL_STATE,
  action: AuthAction,
): AuthState {
  switch (action.type) {
    case '@auth/SIGN_IN_REQUEST':
      return {
        ...state,
        loadingSignInRequest: true,
      };

    case '@auth/SIGN_IN_SUCCESS':
      return {
        ...state,
        loadingSignInRequest: false,
        authorization_token: action.payload.authorization_token,
        user: action.payload.user,
        refreshToken: action.payload.refreshToken,
        errMessage: null,
      };

    case '@auth/SIGN_IN_RESET':
      return {
        ...state,
        loadingSignInRequest: false,
        authorization_token: null,
        user: {} as User,
        errMessage: null,
      };

    case '@auth/SIGN_IN_FAILURE':
      return {
        ...state,
        loadingSignInRequest: false,
        error: true,
        errMessage: action.payload.message,
      };

    default:
      return state;
  }
}
