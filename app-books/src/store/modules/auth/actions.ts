import { action } from 'typesafe-actions';

export function signInRequest({
  email,
  password,
}: {
  email: string;
  password: string;
}) {
  return action('@auth/SIGN_IN_REQUEST', { email, password });
}

export function signInSuccess({ authorization_token, user, refreshToken }: {
    authorization_token: string; user: any; refreshToken: string
  }) {
  return action('@auth/SIGN_IN_SUCCESS', { authorization_token, user, refreshToken });
}

export function signInReset() {
  return action('@auth/SIGN_IN_RESET');
}

export function signInFailure({ message }: { message?: string }) {
  return action('@auth/SIGN_IN_FAILURE', { message });
}
