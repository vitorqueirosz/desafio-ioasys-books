import { ActionType } from 'typesafe-actions';

import * as actions from './actions';

export type AuthAction = ActionType<typeof actions>;

export interface AuthState {
  readonly loadingSignInRequest: boolean;
  readonly error: boolean;
  readonly authorization_token: string | null;
  readonly errMessage?: string | null;
  readonly user: User;
  readonly refreshToken: string | null;
}

export interface User {
  name: string;
  email: string;
  birthdate: string;
  gender: string;
  id: string;
}
