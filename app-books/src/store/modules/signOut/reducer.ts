import { SignOutAction, SignOutState } from './types';

const INTIAL_STATE: SignOutState = {
  error: false,
  errMessage: null,
};

export default function auth(
  state = INTIAL_STATE,
  action: SignOutAction,
): SignOutState {
  switch (action.type) {
    case '@auth/SIGN_OUT_REQUEST':
      return {
        ...state,
      };

    case '@auth/SIGN_OUT_SUCCESS':
      return {
        ...state,
        errMessage: null,
      };

    case '@auth/SIGN_OUT_FAILURE':
      return {
        ...state,
        error: false,
      };

    default:
      return state;
  }
}
