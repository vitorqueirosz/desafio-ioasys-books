import { action } from 'typesafe-actions';

export function signOutRequest() {
  return action('@auth/SIGN_OUT_REQUEST');
}

export function signOutSuccess() {
  return action('@auth/SIGN_OUT_SUCCESS');
}

export function signOutFailure() {
  return action('@auth/SIGN_OUT_FAILURE');
}
