import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

import * as authActions from '../auth/actions';
// import * as actions from './actions';

export function* signOut() {
  try {
    yield put(authActions.signInReset());
  } catch (err) {
    yield put(actions.signOutFailure());
  }
}

export default all([takeLatest('@auth/SIGN_OUT_REQUEST', signOut)]);
