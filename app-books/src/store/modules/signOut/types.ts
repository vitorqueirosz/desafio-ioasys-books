import { ActionType } from 'typesafe-actions';

import * as actions from './actions';

export type SignOutAction = ActionType<typeof actions>;

export interface SignOutState {
  readonly error: boolean;
  readonly errMessage?: string | null;
}

export interface User {
  name: string;
  email: string;
  birthdate: string;
  gender: string;
  id: string;
}
