import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import { pages, components } from './locales';

i18next
  .use(LanguageDetector)
  .init({
    resources: {
      en: {
        translation: {
          pages,
          components,
        },
      },
      pt: {
        translation: pages,
      },
    },
    fallbackLng: 'en',
    debug: false,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18next;
