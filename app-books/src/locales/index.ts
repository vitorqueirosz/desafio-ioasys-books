import * as components from './components';
import * as pages from './pages';

export { pages, components };
