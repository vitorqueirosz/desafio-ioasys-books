// import { shade } from 'polished';
import styled from 'styled-components';

export const Container = styled.button`
    cursor: pointer;
    width: 8.5rem;
    height: 36px;
    border-radius: 44px;
    border: 0;
    background: #fff;

    color: #B22E6F;
    font-weight: bold;
    font-size: 16px;
    position: absolute;
    right: 16px;


    @media (max-width: 700px) {
      width: 6.5rem;
    }
`;
