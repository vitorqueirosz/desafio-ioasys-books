/* eslint-disable react/require-default-props */
import React from 'react';

import * as S from './styles';

interface ToolTipProps {
  publisher?: boolean;
  children: React.ReactNode;
  isOnOver?: boolean;
}

const Tooltip = ({ children, publisher, isOnOver }: ToolTipProps): JSX.Element => (
  <S.Container publisher={publisher} isOnOver={isOnOver}>
    <span>{children}</span>
  </S.Container>
);

export default Tooltip;
