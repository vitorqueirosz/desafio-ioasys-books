import styled, { css } from 'styled-components';

interface TooltipProps {
  publisher?: boolean;
  isOnOver?: boolean;
}

export const Container = styled.div<TooltipProps>`

  display: flex;
  align-items: center;
  width: 100px;
  max-width: 180px;
  position: absolute;
  background: rgba(255, 255, 255, 0.4);
  border-radius: 4px;
  width: 239px;
  height: 48px;
  margin-top: 12px;
  padding-left: 16px;
  transition: opacity 0.6s;

  ${({ publisher }) => publisher && css`
    height: 36px;
    left: 40%;
    bottom: 170%;
    background: rgba(0,0,0,0.7);
    padding: 4px 8px;
    opacity: 0;
    visibility: hidden;
    justify-content: center;
  `}

  ${({ isOnOver }) => isOnOver && css`
    opacity: 1;
    visibility: visible;
  `}

  &::before {
    content: '';

    border-style: solid;
    border-color: rgba(255, 255, 255, 0.4) transparent;
    border-width: 0px 6px 6px 6px;
    bottom: 20px;
    bottom: 100%;
    left: 25%;
    position: absolute;
    transform: translateX(-50%);

    ${({ publisher }) => publisher && css`
      border-color: rgba(0,0,0,0.7) transparent;
      border-width: 6px 6px 0 6px;
      bottom: 20px;
      top: 100%;
      left: 25%;
  `}
  }

  span {
    font-size: 1.6rem;
    color: #fff;
    font-weight: 700;

    ${({ publisher }) => publisher && css`
      font-size: 1.2rem;
    `}

    @media (max-width: 700px) {
      font-size: 1rem;
    }
  }
`;
