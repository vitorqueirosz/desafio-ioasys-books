import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { fetchBookRequest } from '../../../../store/modules/book/actions';
import { Book } from '../../../../store/modules/books/types';

import bookDefault from '../../../../assets/book-default.png';

import * as S from './BookItem.styles';
import Tooltip from '../../../structure/Tooltip';

interface BookItemProps {
  book: Book;
  handleSelectBookItem: () => void;
}

const BookItem = ({ book, handleSelectBookItem }: BookItemProps): JSX.Element => {
  const [isOnOver, setIsOnOver] = useState(false);

  const dispatch = useDispatch();

  const handleBookRequest = useCallback(() => {
    dispatch(fetchBookRequest({
      id: book.id,
    }));
  }, [dispatch, book.id]);

  const handleSelectBook = useCallback(() => {
    handleBookRequest();
    handleSelectBookItem();
  }, [handleSelectBookItem, handleBookRequest]);

  return (
    <S.Container>
      <S.SelectedBookButton onClick={handleSelectBook}>
        <img src={book.imageUrl ? book.imageUrl : bookDefault} alt="bookimage" />
      </S.SelectedBookButton>

      <S.BooksInfoContainer>

        <S.TopContent>
          <S.BookTitle>{book.title}</S.BookTitle>
          {book.authors.map((author) => (
            <S.AuthorTitle key={author}>{author}</S.AuthorTitle>
          ))}
        </S.TopContent>

        <S.BottomContent>
          <S.BookInfoText>
            {book.pageCount}
            {' '}
            paginas
          </S.BookInfoText>
          <S.BookInfoContainer>
            <S.BookInfoText
              publisher
              onMouseOver={() => setIsOnOver((prevState) => !prevState)}
              onMouseLeave={() => setIsOnOver((prevState) => !prevState)}
            >
              Editora
              {' '}
              {book.publisher}
            </S.BookInfoText>

            <Tooltip publisher isOnOver={isOnOver}>{book.publisher}</Tooltip>
          </S.BookInfoContainer>
          <S.BookInfoText>
            Publicado em
            {' '}
            {book.published}
          </S.BookInfoText>
        </S.BottomContent>

      </S.BooksInfoContainer>
    </S.Container>
  );
};

export default BookItem;
