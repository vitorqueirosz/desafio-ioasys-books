import styled from 'styled-components';

interface BookItemProps {
  publisher?: boolean;
}

export const Container = styled.div`
  background: #fff;
  width: 272px;
  height: 160px;
  border-radius: 4px;

  padding: 16px;
  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);

  display: flex;
  align-items: center;
  justify-content: flex-start;

  img {
    width: 81px;
    height: 122px;
  }

  @media (max-width: 700px) {
    width: 400px;
  }
`;

export const SelectedBookButton = styled.button`
  border: none;
  background: transparent;
`;

export const BooksInfoContainer = styled.div`
  margin-left: 16px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
`;

export const BookTitle = styled.h3`
  font-size: 1.2rem;

  @media (max-width: 700px) {
    font-size: 0.8rem;
  }
`;

export const AuthorTitle = styled.p`
  color: #AB2680;
  /* margin: 2px 0; */
  font-weight: 500;
  font-size: 1.2rem;

  @media (max-width: 700px) {
    font-size: 0.8rem;
  }
`;

export const TopContent = styled.div``;

export const BottomContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-self: flex-start;

`;

export const BookInfoText = styled.span<BookItemProps>`
  color: #999999;
  font-size: 1.2rem;

  @media (max-width: 700px) {
    font-size: 0.8rem;
  }

  width: 100%;
  display: -webkit-box;
  max-width: 100%;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const BookInfoContainer = styled.div`
  position: relative;
`;
