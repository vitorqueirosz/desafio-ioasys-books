import React from 'react';

import { FiX } from 'react-icons/fi';

import { useSelector } from 'react-redux';
import { StoreState } from '../../../../store/createStore';
import { Book } from '../../../../store/modules/books/types';
import BookInfosContainer from '../BookInfosContainer';

import * as S from './BookModalItem.styles';
import LoadingItem from '../LoadingBookItem';

interface BookModalItemProps {
  handleCloseModal: () => void;
  book: Book;
}

const BookModalItem = ({ handleCloseModal, book }: BookModalItemProps): JSX.Element => {
  const { loadingBookRequest } = useSelector((state: StoreState) => state.book);

  return (
    <S.Wrapper>
      <S.Container>
        <S.Header>
          <S.CloseButton onClick={handleCloseModal}>
            <FiX size={25} color="#333" />
          </S.CloseButton>
        </S.Header>
        <S.Content>
          <S.BookContainer>
            {loadingBookRequest ? (
              <LoadingItem isLoading={loadingBookRequest} />
            ) : (
              <>
                <img src={book.imageUrl} alt="bookimage" />

                <S.BookAsideContainer>
                  <S.TopContent>
                    <S.BookTitle>{book.title}</S.BookTitle>
                    {book.authors?.length ? (
                      book.authors.map((author, index) => (
                        <S.BookAuthors key={author}>
                          {author}
                          {index < book.authors.length - 1 ? ',\u00A0' : ''}
                        </S.BookAuthors>
                      ))
                    ) : (
                      <S.BookAuthors>
                        {book.author}
                      </S.BookAuthors>
                    )}
                  </S.TopContent>

                  <S.MiddleContent>
                    <S.Title>INFORMAÇÕES</S.Title>

                    <BookInfosContainer title="Páginas" bookData={book.pageCount} />
                    <BookInfosContainer title="Editora" bookData={book.publisher} />
                    <BookInfosContainer title="publicação" bookData={book.published} />
                    <BookInfosContainer title="Idioma" bookData={book.language} />
                    <BookInfosContainer title="ISBN-10" bookData={book.isbn10} />
                    <BookInfosContainer title="ISBN-13" bookData={book.isbn13} />

                  </S.MiddleContent>

                  <S.BottomContent>
                    <S.Title>RESENHA DA EDITORA</S.Title>

                    <S.Description>{book.description}</S.Description>
                  </S.BottomContent>

                </S.BookAsideContainer>
              </>
            )}
          </S.BookContainer>
        </S.Content>
      </S.Container>
    </S.Wrapper>
  );
};

export default BookModalItem;
