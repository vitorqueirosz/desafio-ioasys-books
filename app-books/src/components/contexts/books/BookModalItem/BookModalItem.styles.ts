import styled from 'styled-components';

export const Wrapper = styled.div`

  position: absolute;
  width: 100%;
  height: 100vh;
  display: flex;

  &::before {
    content: "";
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background: rgba(0, 0, 0, 0.6);
    z-index: 4;
  }
`;

export const Container = styled.div`
  position: absolute;
  width: 100vw;

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 700px) {
   min-height: 100vh;
  }

  z-index: 5;
`;

export const Header = styled.header`
  display: flex;
  justify-content: flex-start;
  align-self: flex-end;
  padding: 8px 24px;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;
`;

export const CloseButton = styled.button`
  background: #fff;
  border: none;
  border-radius: 50%;
  width: 32px;
  height: 32px;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const BookContainer = styled.div`
  width: 769px;
  padding: 16px 48px;
  background: #fff;
  height: 560px;
  border-radius: 4px;

  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 700px) {
   flex-direction: column;
   justify-content: flex-start;
   width: 390px;
   height: 100%;
   padding: 24px;
  }
`;

export const BookAsideContainer = styled.aside`
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-left: 4.8rem;
  justify-content: space-between;

  @media (max-width: 700px) {
    margin-left: 0;
  }
`;

export const TopContent = styled.div`

  @media (max-width: 700px) {
    margin-top: 16px;
  }
`;

export const BookTitle = styled.h1`
  font-size: 2.8rem;
  color: #333;
  font-weight: 500;

  @media (max-width: 700px) {
    font-size: 2.4rem;
  }
`;

export const BookAuthors = styled.span`
  color: #AB2680;
  font-size: 1.2rem;

  @media (max-width: 700px) {
    font-size: 1rem;
  }



`;

export const MiddleContent = styled.div`
  width: 100%;

  @media (max-width: 700px) {
    margin-top: 32px;
  }
`;

export const Title = styled.h2`
  font-weight: 500;
  color: #333;

  @media (max-width: 700px) {
    font-size: 1.2rem;
  }
`;

export const MiddleContainer = styled.div`

`;

export const BottomContent = styled.div`
  @media (max-width: 700px) {
    margin-top: 32px;
  }
`;

export const Description = styled.p`
  line-height: 20px;
  color: #999;
  margin-top: 16px;
  font-size: 1.2rem;

  @media (max-width: 700px) {
    font-size: 1rem;
    line-height: 28px;
  }
`;
