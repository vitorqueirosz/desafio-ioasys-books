import styled, { css, keyframes } from 'styled-components';

interface LoadingContainerProps {
  isLoading: boolean;
}

const rotate = keyframes`
    from {
        transform: rotate(0deg);
    }to {
        transform: rotate(360deg);
    }
`;

export const Container = styled.div<LoadingContainerProps>`

  display: flex;
  justify-content: center;
  width: 100%;

  ${(props) => props.isLoading
        && css`
            svg {
                animation: ${rotate} 2s linear infinite;
            }
        `}
`;
