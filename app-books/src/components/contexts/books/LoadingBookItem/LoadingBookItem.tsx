import React from 'react';
import { FaSpinner } from 'react-icons/fa';

import { Container } from './styles';

interface LoadingProps {
  isLoading: boolean;
}

const LoadingItem = ({ isLoading }:LoadingProps): JSX.Element => (
  <Container isLoading={isLoading}>
    <FaSpinner size={25} color="#57bbbc" />
  </Container>
);

export default LoadingItem;
