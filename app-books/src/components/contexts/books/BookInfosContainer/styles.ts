import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;

  margin-top: 4px;

  &:nth-child(2) {
    margin-top: 16px;
  }
`;

export const LeftText = styled.h2`
  font-weight: 500;
  color: #333;

  @media (max-width: 700px) {
    font-size: 1.0rem;
  }

`;
export const RightText = styled.span`
  color: #999;
  font-weight: 400;

  @media (max-width: 700px) {
    font-size: 1.0rem;
  }
`;
