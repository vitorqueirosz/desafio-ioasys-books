import React from 'react';

import { Container, LeftText, RightText } from './styles';

interface BookInfosContainerProps {
  title: string;
  bookData: string;
}

const BookInfosContainer = ({ title, bookData }: BookInfosContainerProps): JSX.Element => (
  <Container>
    <LeftText>{title}</LeftText>
    <RightText>{bookData}</RightText>
  </Container>
);

export default BookInfosContainer;
