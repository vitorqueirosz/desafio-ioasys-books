import React, {
  InputHTMLAttributes,
} from 'react';

import Button from '../../structure/Button';

import {
  Wrapper,
  MainContainer, Container, AsideContainer, InputlLabel, ErrorText,
} from './styles';

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  error?: string;
  isError?: boolean;
  label: string;
  hasButton?: boolean;
  handleAuthRequest?: () => void;
}

const Input: React.FC<IProps> = ({
  error,
  isError,
  hasButton,
  label,
  ...rest
}) => (
  <Wrapper>
    <Container hasButton={hasButton}>
      <MainContainer>
        <InputlLabel>{label}</InputlLabel>

        <input
          {...rest}
        />
      </MainContainer>

      {hasButton && (
        <AsideContainer>
          <Button type="submit">Entrar</Button>
        </AsideContainer>
      )}
    </Container>
    {isError && <ErrorText>{error}</ErrorText>}
  </Wrapper>
);

export default Input;
