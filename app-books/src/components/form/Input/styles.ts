import styled from 'styled-components';

interface IContainerProps {
    isFocused?: boolean;
    isFilled?: boolean;
    hasButton?: boolean;
}

export const Wrapper = styled.div`
  width: 100%;
`;

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  flex: 1;
`;

export const Container = styled.div<IContainerProps>`
    display: flex;
    border-radius: 4px;
    padding: 8px 16px;
    width: 100%;
    justify-content: space-between;
    background: rgba(0, 0, 0, 0.32);

    flex-direction: row;
    align-items: center;

    height: 6.0rem;
    margin-top: 1.6rem;

    svg {
        margin-right: 5px;
        color: #ee4c77;
    }

    input {
        flex: 1;
        height: 100%;
        font-size: 1.6rem;
        background: transparent;
        color: #fff;
        border: 0;

        &::placeholder {
            color: #fff;
        }
    }

    @media (max-width: 700px) {

      height: 4.5rem;

      input {
        font-size: 1.0rem;
        font-weight: 400;
      }
    }
`;

export const ErrorText = styled.span`
  color: #FFCBCB;
  margin: 2px 0;
  font-size: 13px;
`;

export const InputlLabel = styled.span`
  font-size: 1.2rem;
  color: #ddd;

  @media (max-width: 700px) {
    font-size: 0.8rem;
  }
`;

export const AsideContainer = styled.aside`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 9.5rem;
`;
