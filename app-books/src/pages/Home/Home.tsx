import React, { useCallback, useEffect, useState } from 'react';

import { FiLogOut, FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import logoBlack from '../../assets/logo-black.png';
import BookItem from '../../components/contexts/books/BookItem';
import BookModalItem from '../../components/contexts/books/BookModalItem';
import api from '../../services/api';
import { StoreState } from '../../store/createStore';

import { fetchBooksRequest } from '../../store/modules/books/actions';
import { signOutRequest } from '../../store/modules/signOut/actions';

import * as S from './Home.styles';

const Home: React.FC = () => {
  const [t] = useTranslation();

  const [currentPage, setCurrentPage] = useState(1);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [screenSize, setScreenSize] = useState(710);

  const dispatch = useDispatch();

  const { user, authorization_token } = useSelector((state: StoreState) => state.auth);
  const { books, totalPages } = useSelector((state: StoreState) => state.books);

  const { book: fetchedBook } = useSelector((state: StoreState) => state.book);

  useEffect(() => {
    api.defaults.headers.Authorization = `Bearer ${authorization_token}`;
  }, [authorization_token]);

  useEffect(() => {
    setScreenSize(window.innerWidth);
  }, []);

  useEffect(() => {
    dispatch(fetchBooksRequest({ page: 1, amount: 12 }));
  }, [dispatch]);

  const handleNextBookPage = useCallback(() => {
    setCurrentPage((prevState) => prevState + 1);

    dispatch(fetchBooksRequest({ page: currentPage + 1, amount: 12 }));
  }, [dispatch, currentPage]);

  const handlePreviewBookPage = useCallback(() => {
    setCurrentPage((prevState) => prevState - 1);

    dispatch(fetchBooksRequest({ page: currentPage - 1, amount: 12 }));
  }, [dispatch, currentPage]);

  const handleSelectBookItem = useCallback(() => {
    setIsModalOpen((prevState) => !prevState);
  }, []);

  const handleSignOut = () => {
    dispatch(signOutRequest());
  };

  const handleResize = () => {
    setScreenSize(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
  }, []);

  return (
    <>
      <S.Container>

        <S.BackgroundImage />

        <S.Content>
          <S.Header>
            <S.HeaderAside>
              <img src={logoBlack} alt="logo-black" />

              <S.BooksTitle>Books</S.BooksTitle>
            </S.HeaderAside>

            <S.HeaderAside>
              {screenSize > 700 && (
              <S.WelcomeTitle>
                {t('pages.home.enUs.header.title')}
                ,
                <span>{user.name}</span>
              </S.WelcomeTitle>

              ) }
              <S.SignOutButton onClick={handleSignOut}>
                <FiLogOut size={18} color="#333" />
              </S.SignOutButton>
            </S.HeaderAside>
          </S.Header>

          <S.BooksListContainer>

            {books.map((book) => (
              <BookItem key={book.id} book={book} handleSelectBookItem={handleSelectBookItem} />
            ))}
          </S.BooksListContainer>

          {screenSize >= 700 ? (
            <S.PageContainer>
              <S.PageTitle>
                Página
                <span>{currentPage}</span>
                de
                <span>{totalPages}</span>
              </S.PageTitle>

              <S.ActionsContainer>
                <S.ButtonPage onClick={handlePreviewBookPage} disabled={currentPage === 1}>
                  <FiChevronLeft size={18} color={currentPage === 1 ? '#ddd' : '#333'} />
                </S.ButtonPage>

                <S.ButtonPage onClick={handleNextBookPage} disabled={currentPage === 50}>
                  <FiChevronRight size={18} color={currentPage === 50 ? '#ddd' : '#333'} />
                </S.ButtonPage>
              </S.ActionsContainer>
            </S.PageContainer>
          ) : (
            <S.PageContainer>

              <S.ActionsContainer>
                <S.ButtonPage onClick={handlePreviewBookPage} disabled={currentPage === 1}>
                  <FiChevronLeft size={18} color={currentPage === 1 ? '#ddd' : '#333'} />
                </S.ButtonPage>

                <S.PageTitle>
                  Página
                  <span>{currentPage}</span>
                  de
                  <span>{totalPages && Math.ceil(totalPages)}</span>
                </S.PageTitle>

                <S.ButtonPage onClick={handleNextBookPage} disabled={currentPage === 50}>
                  <FiChevronRight size={18} color={currentPage === 50 ? '#ddd' : '#333'} />
                </S.ButtonPage>
              </S.ActionsContainer>
            </S.PageContainer>
          ) }
        </S.Content>
        {isModalOpen && (
          <BookModalItem handleCloseModal={handleSelectBookItem} book={fetchedBook} />
        )}
      </S.Container>
    </>
  );
};

export default Home;
