import styled, { css } from 'styled-components';

import homeBackground from '../../assets/homeBackground.png';

export const Container = styled.div`
  width: 100%;
  min-height: 100vh;

  display: flex;
  background: transparent;
  justify-content: center;
`;

export const BackgroundImage = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;

  background: url(${homeBackground}) no-repeat center;
  background-size: cover;
`;

export const Content = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;

  margin-top: 42px;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 1100px;
`;

export const Header = styled.header`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

`;

export const HeaderAside = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  @media (max-width: 700px) {
    padding: 0 24px;
  }
`;

export const BooksTitle = styled.span`
  margin-left: 5%;
  font-size: 2.8rem;
  color: #333;
  font-weight: 300;


  @media (max-width: 700px) {
    font-size: 2.0rem;
  }
`;

export const WelcomeTitle = styled.p`

  margin-right: 16px;
  font-size: 16px;

  > span {
    font-weight: 500;
    margin-left: 4px;
  }
`;

export const SignOutButton = styled.button`
  background: transparent;
  border: 1px solid rgba(51, 51, 51, 0.2);
  border-radius: 50%;
  width: 32px;
  height: 32px;

  display: flex;
  justify-content: center;
  align-items: center;

`;

export const BooksListContainer = styled.div`
  margin-top: 42px;

  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(3, 1fr);
  grid-gap: 8px;

  @media (max-width: 700px) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr;
  }
`;

export const PageContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  align-self: flex-end;

  margin-top: 16px;

  @media (max-width: 700px) {
    align-self: center;
  }
`;

export const ActionsContainer = styled.div`
  margin-left: 8px;

  @media (max-width: 700px) {
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: row;

   height: 60px;

  }
`;

export const PageTitle = styled.p`

  span {
    font-weight: 500;
    margin:0 4px;
  }

  @media (max-width: 700px) {
    margin: 0 16px;
  }
`;

export const ButtonPage = styled.button`
  background: transparent;
  border: 1px solid rgba(51, 51, 51, 0.2);
  border-radius: 50%;
  width: 32px;
  height: 32px;
  margin-left: 4px;

  @media (max-width: 700px) {
    margin-left: 0;
  }
`;

export const Teste = styled.div`
  position: absolute;
  width: 100%;
  background: rgba(0, 0, 0, 0.4);
  height: 100vh;
`;
