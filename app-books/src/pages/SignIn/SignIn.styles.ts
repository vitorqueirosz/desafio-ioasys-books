import styled from 'styled-components';

import signInBackground from '../../assets/signInBackground.png';

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 0 auto;

  @media (max-width: 700px) {

    justify-content: center;
    align-items: center;
  }
`;

export const BackgroundImage = styled.div`
  width: 100vw;
  height: 100vh;

  background: url(${signInBackground}) no-repeat center;
  background-size: cover;
`;

export const MainContainer = styled.div`
  position: absolute;
  margin-left: 10%;

  @media (max-width: 700px) {
    margin-left: 0;
  }

`;

export const Form = styled.form`
  width: 368px;
  margin-top: 15%;

`;

export const LogosContainer = styled.div`
  flex-direction: row;
  display: flex;
  align-items: center;
`;
export const BooksTitle = styled.span`
  margin-left: 5%;
  font-size: 2.8rem;
  color: #fff;
  font-weight: 300;
`;
