import { useFormik } from 'formik';
import React, { useCallback } from 'react';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Input from '../../components/form/Input';
import * as S from './SignIn.styles';

import logoWhite from '../../assets/logo-white.png';
import { signInRequest } from '../../store/modules/auth/actions';
import Tooltip from '../../components/structure/Tooltip';
import { StoreState } from '../../store/createStore';

interface SignInData {
  email: string;
  password: string
}

const SignIn = (): JSX.Element => {
  const [t] = useTranslation();

  const { errMessage } = useSelector((state: StoreState) => state.auth);

  const schemaValidate = Yup.object().shape({
    email: Yup.string()
      .required('E-mail obrigatório')
      .email('Digite um e-mail válido'),
    password: Yup.string()
      .required('Senha não pode ficar vazia')
      .min(8, () => 'Sua senha está muito pequena'),
  });

  const dispatch = useDispatch();

  const {
    errors,
    handleSubmit,
    handleChange,
    values,
    handleBlur,
    touched,
    setFieldTouched,
  } = useFormik({
    initialValues: { email: '', password: '' },
    onSubmit: (data: SignInData) => handleSignInRequest(data),
    validationSchema: schemaValidate,
  });

  const handleSignInRequest = useCallback((data: SignInData) => {
    const { email, password } = data;
    dispatch(signInRequest({ email, password }));
  }, []);

  return (
    <S.Container>
      <S.BackgroundImage />

      <S.MainContainer>

        <S.LogosContainer>
          <img src={logoWhite} alt="logo-white" />

          <S.BooksTitle>Books</S.BooksTitle>
        </S.LogosContainer>

        <S.Form onSubmit={handleSubmit}>
          <Input
            isError={(!!errors.email && !touched.email && !!values.email)
              || (!!errors.email && !!touched.email && !!values.email)}
            label={t('components.input.enUs.email-label')}
            type="email"
            error={errors.email}
            placeholder="E-mail"
            onChange={handleChange('email')}
            value={values.email}
            onFocus={() => setFieldTouched('email', true)}
            onBlur={() => {
              handleBlur('email');
              setFieldTouched('email', false);
            }}
          />
          <Input
            isError={(!!errors.password && !touched.password && !!values.password)
              || (!!errors.password && !!touched.password && !!values.password)}
            label={t('components.input.enUs.password-label')}
            type="password"
            placeholder="Senha"
            hasButton
            error={errors.password}
            onChange={handleChange('password')}
            value={values.password}
            onFocus={() => setFieldTouched('password', true)}
            onBlur={() => {
              handleBlur('password');
              setFieldTouched('password', false);
            }}
          />
        </S.Form>

        {errMessage && (
          <Tooltip>{errMessage}</Tooltip>
        )}
      </S.MainContainer>

    </S.Container>
  );
};

export default SignIn;
