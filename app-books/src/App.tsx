import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

// eslint-disable-next-line import/no-unresolved
import { I18nextProvider } from 'react-i18next';
import i18next from './i18n';
import GlobalStyle from './styles/global';
import Routes from './routes';
import { persistor, store } from './store';

function App(): JSX.Element {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <I18nextProvider i18n={i18next}>
          <Routes />
        </I18nextProvider>
        <GlobalStyle />
      </PersistGate>
    </Provider>
  );
}

export default App;
