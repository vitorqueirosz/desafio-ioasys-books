import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }

    html, body, #root {
      min-height: 100vh;
    }

    body {
        font-family: 'Heebo', serif;
    }

    #root {
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
    }

    button {
        cursor: pointer;
        outline: none;
    }

    @media (min-width: 700px) {
        :root {
            font-size: 62.5%;
        }
    }

`;
